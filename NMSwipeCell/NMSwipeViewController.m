//
//  NMSwipeViewController.m
//  NMSwipeCell
//
//  Created by Gelan Almagro on 5/6/13.
//  Copyright (c) 2013 Gelan Almagro. All rights reserved.
//

#import "NMSwipeViewController.h"
#import "NMSwipeTableViewCell.h"
#import "UIColor+Expanded.h"
#import <QuartzCore/QuartzCore.h>



@interface NMSwipeViewController ()<NMSwipeTableViewCellDelegate>{
    int count;
    NSIndexPath *movablePath;
    NMSwipeTableViewCellState *_state;
}

@end

@implementation NMSwipeViewController 

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    count = 15;
    _state = malloc(count*sizeof(NMSwipeTableViewCellState));
    memset(_state, NMSwipeTableViewCellStateDefault, count*sizeof(NMSwipeTableViewCellState));//fill all states with NMSwipeTableViewCellStateDefault
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 66;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"swipeCell";
    
    //cell.allowedState states are not reusable for now
    //
    NMSwipeTableViewCell *cell = (NMSwipeTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.delegate  = self;
     cell.allowedState = (NMSwipeTableViewCellStateFarRight | NMSwipeTableViewCellStateFarLeft | NMSwipeTableViewCellStateOutLeft |  NMSwipeTableViewCellStateOutRight | NMSwipeTableViewCellStateDefault);
    
    cell.state = _state[indexPath.row];
    cell.nameLabel.text = [NSString stringWithFormat:@"Cell - %@",[[cell colorForState:cell.state] closestColorName]];
    NSLog(@"Well");
    
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.row == movablePath.row) {
        return YES;
    }
    return NO;
}

- (BOOL) tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
        return NO;
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    if (indexPath.row == movablePath.row) {
        return YES;
    }
    return NO;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

#pragma mark swipeCell Delegates

- (void)swipeTableViewCell:(NMSwipeTableViewCell *)cell willBegingChangingToState:(NSUInteger)state{
    NSLog(@"Changing to Delete");
    //[self.tableView bringSubviewToFront:cell];
    NSIndexPath *path = [self.tableView indexPathForCell:cell];
    _state[path.row] = state;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *str = [[cell colorForState:state] closestColorName];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.nameLabel.text = [NSString stringWithFormat:@"Cell - %@",str];
        });
        
    });
    //cell.nameLabel.text = [NSString stringWithFormat:@"Cell - %@",[[cell colorForState:state] closestColorName]];
    
}


-(CGFloat)swipeTableViewCell:(NMSwipeTableViewCell *)cell widthOffsetForState:(NMSwipeTableViewCellState)state{
    
    
    switch (state) {
        case NMSwipeTableViewCellStateOutLeft:{
            return -60.0;
        }
            break;
        case NMSwipeTableViewCellStateFarLeft:{
            return 90.0;
        }
            break;
        case NMSwipeTableViewCellStateDefault:{
            return 160.0;
        }
            break;
        case NMSwipeTableViewCellStateFarRight:{
            return 230.0;
        }
            break;
        case NMSwipeTableViewCellStateOutRight:{
            return 320.0;
        }
            break;
       
        default:{
            return NSNotFound;
        }
            break;
    }
    return NSNotFound;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NMSwipeTableViewCell *_cell = (NMSwipeTableViewCell*)cell;
    [_cell setState:_state[indexPath.row]];
}

- (void)swipeTableViewCell:(NMSwipeTableViewCell *)cell animationStartedWithState:(NMSwipeTableViewCellState)state{
    NSIndexPath *path = [self.tableView indexPathForCell:cell];
    NSLog(@"%@",path);
    if (!path) {
        return;
    }
    if (state == NMSwipeTableViewCellStateOutLeft) {
        count --;
        int offset = 0;
        int *temp = malloc(count*sizeof(NMSwipeTableViewCellState));
        
        for (int i = 0; i < count; ++i) {
            if (i == path.row)
                offset++;
            else
                temp[i-offset] = (_state)[i];
        }
        memcpy(_state, temp, count);
        [cell.layer removeAllAnimations];
        [self.tableView deleteRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationTop];
    }
}

- (void)swipeTableViewCell:(NMSwipeTableViewCell *)cell didChangeToState:(NSUInteger)state{
   
    switch (state) {
        case NMSwipeTableViewCellStateOutLeft:{
            //count --;
            //[self.tableView deleteRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationFade];
            //[cell.layer removeAllAnimations];
            
        }
            break;
        case NMSwipeTableViewCellStateFarLeft:{
            
        }
            break;
        case NMSwipeTableViewCellStateDefault:{
            
        }
            break;
        case NMSwipeTableViewCellStateFarRight:{
            
        }
            break;
        case NMSwipeTableViewCellStateOutRight:{
            //movablePath = [self.tableView indexPathForCell:cell];
            //[self.tableView setEditing:YES animated:YES];
        }
            break;
            
        default:{
        }
            break;
    }
}

@end
