//
//  CAKeyframeAnimation+parametric.h
//  Family Chef
//
//  Created by Gelan Almagro on 8/20/12.
//  Copyright (c) 2012 Novation Mobile. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

@interface CAKeyframeAnimation (Parametric)

typedef double (^KeyframeParametricBlockFunction)(double);

+ (id)animationWithKeyPath:(NSString *)path
                  function:(KeyframeParametricBlockFunction)block
                 fromValue:(double)fromValue
                   toValue:(double)toValue;

+ (id)animationWithKeyPath:(NSString *)path
                        function:(KeyframeParametricBlockFunction)block
                 fromPoint:(CGPoint)fromValue
                   toPoint:(CGPoint)toValue;

+ (id)animationWithPath:(CGPathRef)curvePath function:(KeyframeParametricBlockFunction)block;
+ (id)animationComboWithKeyPath:(NSString *)path
                        functionX:(KeyframeParametricBlockFunction)blockX functionY:(KeyframeParametricBlockFunction)blockY
                 fromPoint:(CGPoint)fromValue
                   toPoint:(CGPoint)toValue;
@end
