//
//  NMSwipeTableViewCell.m
//  NMSwipeCell
//
//  Created by Gelan Almagro on 5/6/13.
//  Copyright (c) 2013 Gelan Almagro. All rights reserved.
//

#import "NMSwipeTableViewCell.h"
#import "AGView.h"
#import "UIColor+Expanded.h"

#define MIN_ANGLE -0.1
#define MAX_ANGLE 0.1

#define MAINSIZE [UIScreen mainScreen].bounds.size

#define NMSwipeTableViewCellStateOutLeft_Default 0
#define NMSwipeTableViewCellStateFarLeft_Default MAINSIZE.width/4.0
#define NMSwipeTableViewCellStateDefault_Default MAINSIZE.width/2.0
#define NMSwipeTableViewCellStateFarRight_Default MAINSIZE.width*3.0/4.0
#define NMSwipeTableViewCellStateOutRight_Default MAINSIZE.width

@interface NMSwipeTableViewCell () <UIGestureRecognizerDelegate>{
    CGFloat previousAngleOffset;
    CGFloat angle;
}

// Init
- (void)initializer;

- (void)handlePanGestureRecognizer:(UIPanGestureRecognizer *)gesture;

- (CGFloat)angleOffsetForCell;
- (NSTimeInterval)animationDurationWithVelocity:(CGPoint)velocity;
- (NMSwipeTableViewCellState)stateForOffset;
// Delegate
- (void)notifyDelegate;
- (void)setState:(NMSwipeTableViewCellState)state animated:(BOOL)animated withVelocity:(CGFloat)velocity;


@property(nonatomic, assign) NMSwipeTableViewCellAllowedDirections direction;
@property(nonatomic, assign) CGFloat currentOffset;
@property(nonatomic, assign) NMSwipeTableViewCellPossitionState *positionY;
@property(nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;

@end

@implementation NMSwipeTableViewCell

@synthesize positionY;

#pragma mark - Initialization

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self initializer];
        [self setUp];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self initializer];
    }
    return self;
}

- (id)init {
    self = [super init];
    
    if (self) {
        [self initializer];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    UIColor *color = self.contentView.backgroundColor;
    color = [color adjustBrightness:selected];
    [UIView animateWithDuration:0.3 animations:^{
        self.customSelectedView.layer.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5*selected].CGColor;
        self.customSelectedBackgroundView.layer.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.15*selected].CGColor;
    }];
    
    NSLog(@"Color is %f",self.contentView.alpha);
    
    //self.contentView.backgroundColor = [self colorForState:_state];
    // Configure the view for the selected state
}
-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    if (editing) {
        [self setState:_state animated:_state != NMSwipeTableViewCellStateOutRight];
    }
}

- (void)layoutSubviews{
    self.backgroundView.backgroundColor = [self colorForState:_state];
    /*
    self.contentView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.contentView.layer.borderWidth = 2.5;
    self.contentView.layer.cornerRadius = 10.0;
    self.contentView.clipsToBounds = YES;
    self.backgroundColor = [UIColor redColor];*/
    /*
    self.backgroundView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.backgroundView.layer.shadowRadius = 2.0;
    self.backgroundView.layer.shadowOffset = CGSizeMake(0, 1);
    self.backgroundView.layer.shadowOpacity = 0.78;
    */ 
}

- (void)initializer {
    
    _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGestureRecognizer:)];
    _panGestureRecognizer.cancelsTouchesInView = NO;
    _panGestureRecognizer.delaysTouchesBegan = YES;
    [self addGestureRecognizer:_panGestureRecognizer];
    [_panGestureRecognizer setDelegate:self];
        _state = 1;
    angle = NO;
       
}

- (void)setUp{
        self.customSelectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
        self.customSelectedBackgroundView.backgroundColor  = [UIColor clearColor];
        self.backgroundView = [[UIView alloc] initWithFrame:self.bounds];
        self.backgroundView.backgroundColor  = [UIColor lightGrayColor];
        
        UILabel *check = [[UILabel alloc] initWithFrame:(CGRect){10,20,25,25}];
        check.backgroundColor = [UIColor clearColor];
        check.text = @"<";
        
        UILabel *exit = [[UILabel alloc] initWithFrame:(CGRect){310,20,25,25}];
        check.backgroundColor = [UIColor clearColor];
        check.text = @">";
        [self.backgroundView addSubview:check    ];
        [self.backgroundView addSubview:exit];
        
    [self insertSubview:self.customSelectedBackgroundView aboveSubview:self.backgroundView];
    self.contentView = [super contentView];
        self.contentView = [[UIView alloc] initWithFrame:self.bounds];
        self.contentView.backgroundColor = [UIColor grayColor];
        
        UILabel *title = [[UILabel alloc] initWithFrame:(CGRect){10,20,25,25}];
        title.backgroundColor = [UIColor clearColor];
        title.text = @"Test";
        [self.contentView addSubview:title];
}

- (void)setAllowedState:(NMSwipeTableViewCellState)allowedState{
    _allowedState = allowedState;
    positionY = malloc(sizeof(NMSwipeTableViewCellPossitionState)*5);
    positionY[0].state = _allowedState & NMSwipeTableViewCellStateOutRight;
    positionY[1].state = _allowedState & NMSwipeTableViewCellStateFarRight;
    positionY[2].state = _allowedState & NMSwipeTableViewCellStateDefault;
    positionY[3].state = _allowedState & NMSwipeTableViewCellStateFarLeft;
    positionY[4].state = _allowedState & NMSwipeTableViewCellStateOutLeft;
    
    if ([self.delegate respondsToSelector:@selector(swipeTableViewCell:widthOffsetForState:)]) {
        positionY[0].pos = [self.delegate swipeTableViewCell:self widthOffsetForState:NMSwipeTableViewCellStateOutRight]/MAINSIZE.width - 0.5;
        positionY[1].pos = [self.delegate swipeTableViewCell:self widthOffsetForState:NMSwipeTableViewCellStateFarRight]/MAINSIZE.width - 0.5;
        positionY[2].pos = [self.delegate swipeTableViewCell:self widthOffsetForState:NMSwipeTableViewCellStateDefault]/MAINSIZE.width - 0.5;
        positionY[3].pos = [self.delegate swipeTableViewCell:self widthOffsetForState:NMSwipeTableViewCellStateFarLeft]/MAINSIZE.width - 0.5;
        positionY[4].pos = [self.delegate swipeTableViewCell:self widthOffsetForState:NMSwipeTableViewCellStateOutLeft]/MAINSIZE.width - 0.5;
    } else {
        positionY[0].pos = NMSwipeTableViewCellStateOutLeft_Default/MAINSIZE.width - 0.5;
        positionY[1].pos = NMSwipeTableViewCellStateFarLeft_Default/MAINSIZE.width - 0.5;
        positionY[2].pos = NMSwipeTableViewCellStateDefault_Default/MAINSIZE.width - 0.5;
        positionY[3].pos = NMSwipeTableViewCellStateFarRight_Default /MAINSIZE.width - 0.5;
        positionY[4].pos = NMSwipeTableViewCellStateOutRight_Default/MAINSIZE.width - 0.5;
    }
}

- (CGFloat)currentOffset{
    _currentOffset = self.contentView.layer.frame.origin.x/CGRectGetWidth(self.bounds);
    if (_currentOffset < -1.0) _currentOffset = -1.0;
    else if (_currentOffset > 1.0) _currentOffset = 1.0;
    return _currentOffset;
}

#pragma mark - Handle Gestures

- (void)handlePanGestureRecognizer:(UIPanGestureRecognizer *)gesture {
    [self.contentView.layer removeAllAnimations];
    UIGestureRecognizerState state = [gesture state];
    CGPoint translation = [gesture translationInView:self];
    CGPoint velocity = [gesture velocityInView:self];
    CGFloat percentage = [self currentOffset];
    //NSTimeInterval animationDuration = [self animationDurationWithVelocity:velocity];
    if (state == UIGestureRecognizerStateBegan) {
            angle = 0;
        NSLog(@"Started Current Offset =%f",_currentOffset);
    }
    else if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged) {
        if (velocity.x>0) {
             angle += 0.005*fabs(velocity.x/(MAINSIZE.width*1.5));
        } else {
             angle -= 0.005*fabs(velocity.x/(MAINSIZE.width*1.5));
        }
           
        if (angle > MAX_ANGLE) {
            angle = MAX_ANGLE;
        }
        if (angle < -MAX_ANGLE) {
            angle = -MAX_ANGLE;
        }
        self.contentView.layer.position = (CGPoint){self.contentView.layer.position.x + translation.x, self.contentView.layer.position.y};
        
        self.contentView.transform = CGAffineTransformMakeRotation(angle);

        _state = [self stateForOffset];
        //[self animateWithOffset:CGRectGetMinX(self.contentView.frame)];
        [gesture setTranslation:CGPointZero inView:self];
    }
    else if (state == UIGestureRecognizerStateEnded || state == UIGestureRecognizerStateCancelled) {
        
        [self setState:[self stateForOffset] animated:YES withVelocity:velocity.x/self.bounds.size.width];
        
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer == _panGestureRecognizer) {
        UIScrollView *superview = (UIScrollView *) self.superview;
        CGPoint translation = [(UIPanGestureRecognizer *) gestureRecognizer translationInView:superview];
        
        // Make sure it is scrolling horizontally
        return ((fabs(translation.x) / fabs(translation.y) > 1) ? YES : NO && (superview.contentOffset.y == 0.0 && superview.contentOffset.x == 0.0));
    }
    return NO;
}

- (NMSwipeTableViewCellState)stateForOffset{
    CGFloat closestDistance = 100000.000;
    int index = -1;
    for (int i = 0; i < 5; i++) {
        CGFloat diff = fabsf(positionY[i].pos - _currentOffset);
    
        if (diff < closestDistance && positionY[i].state != 0) {
                
                closestDistance = diff;
                index = i;
                
            
        }
    }
    
    if (index == - 1) {
        return 0;
    }
    if (_state != positionY[index].state && [self.delegate respondsToSelector:@selector(swipeTableViewCell:willBegingChangingToState:)]) {
            
            [self.delegate swipeTableViewCell:self willBegingChangingToState:positionY[index].state];
        [UIView animateWithDuration:0.4 animations:^{
        self.backgroundView.layer.backgroundColor = [self colorForState:positionY[index].state].CGColor;
        }];
    }
         _state = positionY[index].state;
    return _state;
    
}

- (NMSwipeTableViewCellState)nextStateForOffsetWithVelocity:(CGFloat)velocity{
    
    switch (_state) {
        case NMSwipeTableViewCellStateDefault:{
            
        }
            break;
        case NMSwipeTableViewCellStateFarLeft:{
            
        }
            break;
        default:
            break;
    }
}

- (void)setState:(NMSwipeTableViewCellState)state{
    [self setState:state animated:NO];
}
- (void)setState:(NMSwipeTableViewCellState)state animated:(BOOL)animated{
    NSLog(@"Animated = %i",animated);
    [self setState:state animated:animated withVelocity:0];
    
}

- (UIColor*)colorForState:(NMSwipeTableViewCellState)state{
    NSLog(@"COLOR FOR STATE");
    switch (state) {
        case NMSwipeTableViewCellStateOutRight:{
            return [UIColor colorWithRed:.93 green:.08 blue:.36 alpha:1.0];
        }
            break;
        case NMSwipeTableViewCellStateFarRight:{
            return [UIColor colorWithRed:.49 green:.77 blue:.46 alpha:1.0];
        }
            break;
        case NMSwipeTableViewCellStateDefault:{
            return [UIColor colorWithRed:.44 green:.44 blue:.44 alpha:1.0];
        }
            break;
        case NMSwipeTableViewCellStateFarLeft:{
            return [UIColor colorWithRed:.95 green:.42 blue:.31 alpha:1.0];
        }
            break;
        case NMSwipeTableViewCellStateOutLeft:{
            return [UIColor colorWithRed:.77 green:0.16 blue:0.11 alpha:1.0];
        }
            break;
            
        default:
            break;
    }
    return [UIColor colorWithRed:.44 green:.44 blue:.44 alpha:1.0];
}

- (CGPoint)pointForState:(NMSwipeTableViewCellState)state{
    CGFloat width = 0;
    if ([self.delegate respondsToSelector:@selector(swipeTableViewCell:widthOffsetForState:)]) {
        width = [self.delegate swipeTableViewCell:self widthOffsetForState:state];
        
    }
    if (width < MAINSIZE.width*2) {
        return (CGPoint){width,self.contentView.layer.position.y};
    }
    switch (state) {
        case NMSwipeTableViewCellStateDefault:{
            return (CGPoint){NMSwipeTableViewCellStateDefault_Default,self.contentView.layer.position.y};
        }
            break;
        case NMSwipeTableViewCellStateOutLeft:{
            return (CGPoint){NMSwipeTableViewCellStateOutLeft_Default,self.contentView.layer.position.y};
        }
            break;
        case NMSwipeTableViewCellStateFarLeft:{
            return (CGPoint){NMSwipeTableViewCellStateFarLeft_Default,self.contentView.layer.position.y};
        }
            break;
        
        case NMSwipeTableViewCellStateFarRight:{
            return (CGPoint){NMSwipeTableViewCellStateFarRight_Default,self.contentView.layer.position.y};
        }
            break;
        case NMSwipeTableViewCellStateOutRight:{
            return (CGPoint){NMSwipeTableViewCellStateOutRight_Default,self.contentView.layer.position.y};
        }
            break;
        default:
            return (CGPoint){self.contentView.frame.size.width/2.0,self.contentView.layer.position.y};
            break;
    }

   
}

- (void)setState:(NMSwipeTableViewCellState)state animated:(BOOL)animated withVelocity:(CGFloat)velocity{
    _state = state;
    if ([self.delegate respondsToSelector:@selector(swipeTableViewCell:animationStartedWithState:)] && !self.editing) {
        [self.delegate swipeTableViewCell:self animationStartedWithState:_state];
    }
    //CGFloat duration = velocity/4.0;
    

    CGPoint toPoint = [self pointForState:state];
    _currentOffset = toPoint.x/CGRectGetWidth(self.bounds)-0.5;
    //CGFloat angle = _currentOffset*MAX_ANGLE;
    //if (state == 1) {
    //    angle = 0;
    //}
    //NSLog(@"Next STATE %i and %@ angle %f from offeset = %f",state,NSStringFromCGPoint([self pointForState:state]),angle*180/M_PI,_currentOffset);
    
    [AGView AGanimateWithDuration:0.9*animated delay:0 animations:^{
        [self.contentView AGRunAction:[self.contentView AGMoveTo:toPoint withParametricFunction:AGElasticEaseOut(0.7)]];
        [self.contentView AGRunAction:[self.contentView AGRotateTo:0 withParametricFunction:AGElasticEaseOut(0.7)]];
    } completion:^(BOOL finished) {
        //self.contentView.layer.position = toPoint;
        if (!self.editing) {
            if ([self.delegate respondsToSelector:@selector(swipeTableViewCell:didChangeToState:)]) {
                if ([self.delegate respondsToSelector:@selector(swipeTableViewCell:animationEndedWithState:)]) {
                    [self.delegate swipeTableViewCell:self animationEndedWithState:_state];
                }
                [self.delegate swipeTableViewCell:self didChangeToState:_state];
            }
        }
        
    }];
    
}


@end
