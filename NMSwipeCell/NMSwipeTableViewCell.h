//
//  NMSwipeTableViewCell.h
//  NMSwipeCell
//
//  Created by Gelan Almagro on 5/6/13.
//  Copyright (c) 2013 Gelan Almagro. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NMSwipeTableViewCell;

 enum{
    NMSwipeTableViewCellStateOutNone = 0,
    NMSwipeTableViewCellStateOutLeft = 1 << 0,
    NMSwipeTableViewCellStateFarLeft = 1 << 1,
    NMSwipeTableViewCellStateDefault = 1 << 2,
    NMSwipeTableViewCellStateFarRight = 1 << 3,
    NMSwipeTableViewCellStateOutRight = 1 << 4
};
typedef NSUInteger NMSwipeTableViewCellState;

typedef enum{
    NMSwipeTableViewCellStateDirectionLeft,
    NMSwipeTableViewCellStateDirectionRight
    //NMSwipeTableViewCellState,
}NMSwipeTableViewCellAllowedDirections;

typedef struct{
    NMSwipeTableViewCellState state;
    CGFloat pos;
}NMSwipeTableViewCellPossitionState;


@protocol NMSwipeTableViewCellDelegate <NSObject>
- (void)swipeTableViewCell:(NMSwipeTableViewCell*)cell didChangeToState:(NSUInteger)state;
- (void)swipeTableViewCell:(NMSwipeTableViewCell*)cell willBegingChangingToState:(NSUInteger)state;

@optional
- (CGFloat)swipeTableViewCell:(NMSwipeTableViewCell*)cell widthOffsetForState:(NMSwipeTableViewCellState)state;
- (void)swipeTableViewCell:(NMSwipeTableViewCell*)cell animationEndedWithState:(NMSwipeTableViewCellState)state;
- (void)swipeTableViewCell:(NMSwipeTableViewCell*)cell animationsForState:(NMSwipeTableViewCellState)state;
- (void)swipeTableViewCell:(NMSwipeTableViewCell*)cell animationStartedWithState:(NMSwipeTableViewCellState)state;
- (void)swipeTableViewCell:(NMSwipeTableViewCell*)cell startedSwipingToDirection:(int)direction;
@end

@interface NMSwipeTableViewCell : UITableViewCell

@property (nonatomic, unsafe_unretained) IBOutlet id <NMSwipeTableViewCellDelegate> delegate;
@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (nonatomic, strong) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) IBOutlet UIView *customSelectedBackgroundView;
@property (nonatomic, strong) IBOutlet UIView *customSelectedView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic, assign) NMSwipeTableViewCellState state;
@property (nonatomic, assign) NMSwipeTableViewCellState allowedState;
@property (nonatomic, strong) UIColor *selectedColor;
@property (nonatomic, strong) UIColor *unselectedColor;

@property (nonatomic, assign) CGFloat paddingForWidth;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withStates:(int)state;
- (UIColor*)colorForState:(NMSwipeTableViewCellState)state;
- (void)setState:(NMSwipeTableViewCellState)state animated:(BOOL)animated;

@end
