//
//  NMSwipeViewController.h
//  NMSwipeCell
//
//  Created by Gelan Almagro on 5/6/13.
//  Copyright (c) 2013 Gelan Almagro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMSwipeViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
