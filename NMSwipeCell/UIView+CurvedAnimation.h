//
//  UIView+CurvedAnimation.h
//  Family Chef
//
//  Created by Gelan Almagro on 8/21/12.
//  Copyright (c) 2012 Novation Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAKeyframeAnimation+parametric.h"


typedef struct
{
    CGFloat x,y,z;
}EulerAngle;

typedef struct{
    NSTimeInterval start, duration;
}AGTiming;


#pragma mark PARAMETRIC FUNCTIONS
#pragma mark ---

#pragma mark BOUNCE EASING: exponentially decaying parabolic bounce

static KeyframeParametricBlockFunction AGBounceEaseOut = ^double(double time) {
    double t = time;
    if (t < 1 / 2.75) {
        return 7.5625f * t * t;
    }
    else if (t < 2 / 2.75) {
        t -= 1.5f / 2.75f;
        return 7.5625f * t * t + 0.75f;
    }
    else if (t < 2.5 / 2.75) {
        t -= 2.25f / 2.75f;
        return 7.5625f * t * t + 0.9375f;
    }
    t -= 2.625f / 2.75f;
    return 7.5625f * t * t + 0.984375f;
};


static inline int
fancyNumber(int n){
    int nr = 1;
    for (int i = 1; i<n+1; i++) {
        nr *= i;
    }
    
    return nr;
}


static inline KeyframeParametricBlockFunction
AGBounceEaseInWithSteps(int s){
    
    //Block_copy(nil);
    KeyframeParametricBlockFunction r = ^double(double t) {
        double newT = 0;
        float st = 0.6 / s;
        t = t - 1;
        newT = -powf(2, 10 * t) * sinf( (t) *M_PI * 2);
        
        return newT < 0 && newT > 1 ? -newT : newT;
    };    return  r;
}

static inline KeyframeParametricBlockFunction
AGBounceEaseOutWithSteps(int s){
   
    return ^double(double t) {
        double newT = 0;
        float st = 0.6 / s;
        newT = powf(2, -10 * t) * atanf( (t-st) * 2 * M_PI / 0.6) + 1;
        
        return newT;
        return newT < 0 && newT > 1 ? -newT : newT;
    };
}



static KeyframeParametricBlockFunction AGBounceEaseIn = ^double(double t) {
    KeyframeParametricBlockFunction block = AGBounceEaseOut;
    return 1 - block(1.0 - t);
};

static KeyframeParametricBlockFunction AGBounceEaseInOut = ^double(double t) {

    if  (t < 0.5) {
        KeyframeParametricBlockFunction block = AGBounceEaseIn;

        return block(t * 2.0)*0.5;
    } else {
        KeyframeParametricBlockFunction block = AGBounceEaseOut;

        return block(t*2.0 - 1.0)*0.5 + 0.5;
    }
};

static inline KeyframeParametricBlockFunction
AGElasticEaseIn(float p){
    if (p == 0) {
        p =0.01;
    }
    //Block_copy(nil);
    KeyframeParametricBlockFunction r = ^double(double t) {
        double newT = 0;
        float s = p / 4;
        t = t - 1;
        newT = -powf(2, 10 * t) * sinf( (t-s) *M_PI * 2 / p);
        return newT;
    };
    return  r;
}



static inline KeyframeParametricBlockFunction
AGElasticEaseOut(float p){
    if (p == 0) {
        p =0.01;
    }
    return ^double(double t) {
        double newT = 0;
        float s = p / 4;
        newT = powf(2, -10 * t) * sinf( (t-s) * 2 * M_PI / p) + 1;
        return newT;
    };
}



static inline KeyframeParametricBlockFunction
AGElasticEaseInOut(float p){
    if (p == 0) {
        p =0.01;
    }
    return ^double(double time) {
        CGFloat t = time;
        double newT = 0;
        t = t * 2;
        float s = p / 4;

        t = t -1;
        if( t < 0 )
            newT = -0.5f * powf(2, 10 * t) * sinf((t - s) * M_PI * 2 / p);
        else
            newT = powf(2, -10 * t) * sinf((t - s) * M_PI * 2 / p) * 0.5f + 1;
        return newT;
    };
}

///

static inline KeyframeParametricBlockFunction
AGElasticCosEaseIn(float p){
    if (p == 0) {
        p =0.01;
    }
    //Block_copy(nil);
    KeyframeParametricBlockFunction r = ^double(double t) {
        double newT = 0;
        float s = p / 4;
        t = t - 1;
        newT = -powf(2, 10 * t) * cosf( (t-s) *M_PI * 2 / p);
        return newT;
    };
    return  r;
}



static inline KeyframeParametricBlockFunction
AGElasticCosEaseOut(float p){
    if (p == 0) {
        p =0.01;
    }
    return ^double(double t) {
        double newT = 0;
        float s = p / 4;
        newT = powf(2, -10 * t) * cosf( (t-s) * 2 * M_PI / p) + 1;
        return newT;
    };
}



static inline KeyframeParametricBlockFunction
AGElasticCosEaseInOut(float p){
    if (p == 0) {
        p =0.01;
    }
    return ^double(double time) {
        CGFloat t = time;
        double newT = 0;
        t = t * 2;
        float s = p / 4;
        
        t = t -1;
        if( t < 0 )
            newT = -0.5f * powf(2, 10 * t) * cosf((t - s) * M_PI * 2 / p);
        else
            newT = powf(2, -10 * t) * cosf((t - s) * M_PI * 2 / p) * 0.5f + 1;
        return newT;
    };
}

static KeyframeParametricBlockFunction AGLinearEase = ^double(double t) {
    return t;
};

static KeyframeParametricBlockFunction AGQuadEaseIn = ^double(double t) {
    return t*t;
};

static KeyframeParametricBlockFunction AGQuadEaseOut = ^double(double t) {
    return -1.0 * t * (t-2);
};

static KeyframeParametricBlockFunction AGQuadEaseInOut = ^double(double t) {
    if ((t/2) < 1) return 1/2*t*t;
    return -1/2 * ((--t)*(t-2) - 1);
};

static KeyframeParametricBlockFunction AGCubicEaseIn = ^double(double t) {
    return t*t*t;
};

static KeyframeParametricBlockFunction AGCubicEaseOut = ^double(double t) {
    return ((t=t/-1)*t*t + 1);
};

static KeyframeParametricBlockFunction AGCubicEaseInOut = ^double(double t) {
    if ((t/2) < 1) return 1/2*t*t*t;
    return 1/2*((t-=2)*t*t + 2);
};


typedef enum{
    kCA_BounceIn,
    kCA_BounceOut,
    kCA_BouceInOut,
    kCA_ElasticIn,
    kCA_ElasticOut,
    kCA_ElasticInOut,
    kCA_Normal,
    kCA_OverShot,
    kCA_UnderShot,

}CACurvedAnimationFuction;
typedef enum{
    
    kKeyRotation,
    kKeyScale,
    kKeyScaleX,
    kKeyScaleY,
    kKeyPositionX,
    kKeyPositionY,
    kKeyFade,
    
}CAKeyPath;

@interface UIView (CurvedAnimation)

-(void)animationWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay easeOptions:(CACurvedAnimationFuction)easeOptions forKeyPath:(CAKeyPath)keyPath fromValue:(float)fromValue toValue:(float)toValue started:(void (^)(void))started completion:(void (^)(void))completion;

-(void)animationWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay easeOptions:(CACurvedAnimationFuction)easeOptions forKeyPath:(CAKeyPath)keyPath fromPoint:(CGPoint)fromValue toValue:(CGPoint)toValue started:(void (^)(void))started completion:(void (^)(void))completion;

typedef struct{
    CGFloat sx,sy;
}CGScale;

@property (nonatomic, assign) CGFloat angle;
@property (nonatomic, assign) CGFloat scale;
@property (nonatomic, assign) CGPoint position;
@property (nonatomic, assign) CGScale xyScale;


+ (void)AGanimateWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay animations:(void (^)(void))animations completion:(void (^)(BOOL finished))completion;

+ (void)AGTransitionFromView:(UIView *)fromView toView:(UIView*)toView duration:(NSTimeInterval)duration  animations:(void (^)(void))animations completion:(void (^)(BOOL))completion;

- (CAAnimation*)AGMoveTo:(CGPoint)destinationPoint;
- (CAAnimation*)AGMoveTo:(CGPoint)destinationPoint withParametricFunction:(KeyframeParametricBlockFunction)function;
- (CAAnimation*)AGMoveTo:(CGPoint)destinationPoint withParametricFunction:(KeyframeParametricBlockFunction)function duration:(float)duration;
- (CAAnimation*)AGMoveTo:(CGPoint)destinationPoint withParametricFunction:(KeyframeParametricBlockFunction)function timing:(AGTiming)timing;
- (CAAnimation*)AGMoveTo:(CGPoint)destinationPoint withParametricComboFunctionX:(KeyframeParametricBlockFunction)functionX functionY:(KeyframeParametricBlockFunction)functionY timing:(AGTiming)timing;


- (CAAnimation*)AGRotateTo:(CGFloat)angle;
- (CAAnimation*)AGRotateTo:(CGFloat)angle withParametricFunction:(KeyframeParametricBlockFunction)function;
- (CAAnimation*)AGRotateTo:(CGFloat)angle withParametricFunction:(KeyframeParametricBlockFunction)function duration:(float)duration;
- (CAAnimation*)AGRotateTo:(CGFloat)angle withParametricFunction:(KeyframeParametricBlockFunction)function timing:(AGTiming)timing;

- (CAAnimation*)AGScaleTo:(CGFloat)scale;
- (CAAnimation*)AGScaleTo:(CGFloat)scale withParametricFunction:(KeyframeParametricBlockFunction)function;
- (CAAnimation*)AGScaleTo:(CGFloat)scale withParametricFunction:(KeyframeParametricBlockFunction)function duration:(float)duration;
- (CAAnimation*)AGScaleTo:(CGFloat)scale withParametricFunction:(KeyframeParametricBlockFunction)function timing:(AGTiming)timing;

- (CAAnimation*)AGFadeTo:(CGFloat)fade WithParametricFunction:(KeyframeParametricBlockFunction)function duration:(float)duration;
- (CAAnimationGroup*)AG3DTransformTo:(CATransform3D)transform WithParametricFunction:(KeyframeParametricBlockFunction)function;
- (CAAnimationGroup*)AG3DTransformWithAngles:(EulerAngle)angles WithParametricFunction:(KeyframeParametricBlockFunction)function;

- (CAAnimation*)AGMoveAlongPath:(CGPathRef)path;
- (CAAnimation*)AGMoveAlongPath:(CGPathRef)path withParametricFunction:(KeyframeParametricBlockFunction)function;
- (CAAnimation*)AGMoveAlongPath:(CGPathRef)path withParametricFunction:(KeyframeParametricBlockFunction)function duration:(float)duration;
- (CAAnimation*)AGMoveAlongPath:(CGPathRef)path withParametricFunction:(KeyframeParametricBlockFunction)function timing:(AGTiming)timing;

- (CAAnimation*)AG3DTransformTo:(CATransform3D)transform;
- (void)AGFadeIn;
- (void)AGFadeOut;

- (void)setAngle:(CGFloat)tangle;
- (void)setScale:(CGFloat)scale;
- (void)setPosition:(CGPoint)position;
- (void)setXyScale:(CGScale)xyScale;
- (CGFloat)angle;
- (CGFloat)scale;
- (CGPoint)position;
- (CGScale)xyScale;

- (void)AGRunAction:(id)anim;
- (void)AGRunAction:(CAAnimation*)anim withTiming:(AGTiming)timing;

- (CAAnimationGroup*)AGSequence:(NSArray*)arrayOfActions;
- (void)AGRunAction:(id)anim repeating:(int)repetition;
- (void)AGRunAction:(id)anim repeating:(int)repetition withKey:(NSString*)key;
- (CAAnimationGroup*)AGSequence:(NSArray*)arrayOfActions durationAutoMode:(BOOL)mode;

@end
