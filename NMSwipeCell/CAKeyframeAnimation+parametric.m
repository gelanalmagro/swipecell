//
//  CAKeyframeAnimation+parametric.m
//  Family Chef
//
//  Created by Gelan Almagro on 8/20/12.
//  Copyright (c) 2012 Novation Mobile. All rights reserved.
//

#import "CAKeyframeAnimation+parametric.h"

@implementation CAKeyframeAnimation (Parametric)

+ (id)animationWithKeyPath:(NSString *)path
                  function:(KeyframeParametricBlockFunction)block
                 fromValue:(double)fromValue
                   toValue:(double)toValue {
    // get a keyframe animation to set up
    CAKeyframeAnimation *animation =
    [CAKeyframeAnimation animationWithKeyPath:path];
    // break the time into steps
    //  (the more steps, the smoother the animation)
    NSUInteger steps = 100;
    NSMutableArray *values = [NSMutableArray arrayWithCapacity:steps];
    double time = 0.0;
    double timeStep = 1.0 / (double)(steps - 1);
    for(NSUInteger i = 0; i < steps; i++) {
        double value = fromValue + (block(time) * (toValue - fromValue));
        [values addObject:[NSNumber numberWithDouble:value]];
        time += timeStep;
    }
    // we want linear animation between keyframes, with equal time steps
    animation.calculationMode = kCAAnimationLinear;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];;
    // set keyframes and we're done
    [animation setValues:values];
    return(animation);
}

+ (id)animationComboWithKeyPath:(NSString *)path
                      functionX:(KeyframeParametricBlockFunction)blockX functionY:(KeyframeParametricBlockFunction)blockY
                      fromPoint:(CGPoint)fromValue
                        toPoint:(CGPoint)toValue{
    CAKeyframeAnimation *animation =
    [CAKeyframeAnimation animationWithKeyPath:path];
    // break the time into steps
    //  (the more steps, the smoother the animation)
    NSUInteger steps = 100;
    NSMutableArray *values = [NSMutableArray arrayWithCapacity:steps];
    double time = 0.0;
    double timeStep = 1.0 / (double)(steps - 1);
    
    for(NSUInteger i = 0; i < steps; i++) {
        double xvalue = fromValue.x + (blockX(time) * (toValue.x - fromValue.x));
        double yvalue = fromValue.y + (blockY(time) * (toValue.y - fromValue.y));
        
        [values addObject:[NSValue valueWithCGPoint:CGPointMake(xvalue, yvalue)]];
        time += timeStep;
    }
    // we want linear animation between keyframes, with equal time steps
    animation.calculationMode = kCAAnimationLinear;
    // set keyframes and we're done
    [animation setValues:values];
    return(animation);
    
}
+ (id)animationWithPath:(CGPathRef)curvePath function:(KeyframeParametricBlockFunction)block{
    
    
    // get a keyframe animation to set up
    CAKeyframeAnimation *animation =
    [CAKeyframeAnimation animationWithKeyPath:@"position"];
    
    animation.path = curvePath;
    //animation.rotationMode = kCAAnimationRotateAuto;
    // we want linear animation between keyframes, with equal time steps
    animation.calculationMode = kCAAnimationLinear;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];;
    // set keyframes and we're done
    \
    return(animation);
}


+ (id)animationWithKeyPath:(NSString *)path
                  function:(KeyframeParametricBlockFunction)block
                 fromPoint:(CGPoint)fromValue
                   toPoint:(CGPoint)toValue {
        // get a keyframe animation to set up
    CAKeyframeAnimation *animation =
    [CAKeyframeAnimation animationWithKeyPath:path];
        // break the time into steps
        //  (the more steps, the smoother the animation)
    NSUInteger steps = 100;
    NSMutableArray *values = [NSMutableArray arrayWithCapacity:steps];
    double time = 0.0;
    double timeStep = 1.0 / (double)(steps - 1);

    for(NSUInteger i = 0; i < steps; i++) {
        double xvalue = fromValue.x + (block(time) * (toValue.x - fromValue.x));
        double yvalue = fromValue.y + (block(time) * (toValue.y - fromValue.y));

        [values addObject:[NSValue valueWithCGPoint:CGPointMake(xvalue, yvalue)]];
        time += timeStep;
    }
        // we want linear animation between keyframes, with equal time steps
    animation.calculationMode = kCAAnimationLinear;
        // set keyframes and we're done
    [animation setValues:values];
    return(animation);
}

@end
