//
//  UIView+CurvedAnimation.m
//  Family Chef
//
//  Created by Gelan Almagro on 8/21/12.
//  Copyright (c) 2012 Novation Mobile. All rights reserved.
//

#import "UIView+CurvedAnimation.h"

#define RAD(s)  s*M_PI/180.0
#define DEG(s)  s*180.0/M_PI
#define CCW NO
#define CW YES

static inline NSString*
NSStringFromCATransform3D(CATransform3D t){
    return [NSString stringWithFormat:@"\n\n[%f,%f,%f,%f]\n[%f,%f,%f,%f]\n[%f,%f,%f,%f]\n[%f,%f,%f,%f]\n",t.m11,t.m12,t.m13,t.m14,t.m21,t.m22,t.m23,t.m24,t.m31,t.m32,t.m33,t.m34,t.m41,t.m42,t.m43,t.m44];
}
@implementation UIView (CurvedAnimation)

void (^completed)(void);

+ (EulerAngle)getAnglesFromMatrix:(CATransform3D)tm{
    EulerAngle ea;
    CGFloat za,xa,ya,_za,_xa,_ya;/*
    za = atan2(transform.m12, transform.m11);
    ya = atan2(sy,transform.m13);
    xa = atan2(transform.m23, transform.m33);
    _za = atan2(t3d.m12, t3d.m11);
    _ya = atan2(syt,t3d.m13);
    _xa = atan2(t3d.m23, t3d.m33);
                                  Phi = atan2(T(2,3), T(3,3))
                                  Theta = asin(-T(1,3))
                                  Psi = atan2(-T(1,2), T(1,1))
                                  
                                  */
    ea.y = asin(-tm.m13);
    NSLog(@"%@",NSStringFromCATransform3D(tm));
    
    ea.x = -atan2(tm.m23, tm.m33);
    ea.z = atan2(tm.m12, tm.m11);
    return ea;
}
/* Convert matrix to Euler angles (in radians). */

+ (void)AGanimateWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay animations:(void (^)(void))animations completion:(void (^)(BOOL finished))completion{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [CATransaction begin];
        [CATransaction setValue:[NSNumber numberWithFloat:duration] forKey:kCATransactionAnimationDuration];
        [CATransaction setCompletionBlock:^{
            completion(YES);
        }];
        
        
        animations();
        
        [CATransaction commit];
    });
    
}
+ (void)AGTransitionFromView:(UIView *)fromView toView:(UIView*)toView duration:(NSTimeInterval)duration  animations:(void (^)(void))animations completion:(void (^)(BOOL))completion{
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat:duration] forKey:kCATransactionAnimationDuration];
    [CATransaction setCompletionBlock:^{
        completion(YES);
    }];
    animations();
    [CATransaction commit];
}

#pragma mark AGMove
#pragma mark ---

- (CAAnimation*)AGMoveTo:(CGPoint)destinationPoint{
    return [self AGMoveTo:destinationPoint withParametricFunction:AGLinearEase timing:(AGTiming){0,0}];
}
- (CAAnimation*)AGMoveTo:(CGPoint)destinationPoint withParametricFunction:(KeyframeParametricBlockFunction)function duration:(float)duration{
    return [self AGMoveTo:destinationPoint withParametricFunction:function timing:(AGTiming){0,duration}];
}
- (CAAnimation*)AGMoveTo:(CGPoint)destinationPoint withParametricFunction:(KeyframeParametricBlockFunction)function{
    return [self AGMoveTo:destinationPoint withParametricFunction:function timing:(AGTiming){0,0}];
}

- (CAAnimation*)AGMoveTo:(CGPoint)destinationPoint withParametricFunction:(KeyframeParametricBlockFunction)function timing:(AGTiming)timing{
    CAAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"position" function:function fromPoint:self.layer.position toPoint:destinationPoint];
    anim.fillMode = kCAFillModeForwards;
    anim.removedOnCompletion = YES;
    if (!(timing.start < 0.01 && timing.duration < 0.01)) {
        anim.duration = timing.duration;
        anim.beginTime = timing.start;
    }
    self.position = destinationPoint;
    self.layer.position = destinationPoint;
    return anim;
}

- (CAAnimation*)AGMoveTo:(CGPoint)destinationPoint withParametricComboFunctionX:(KeyframeParametricBlockFunction)functionX functionY:(KeyframeParametricBlockFunction)functionY timing:(AGTiming)timing{
    CAAnimation *anim = [CAKeyframeAnimation animationComboWithKeyPath:@"position" functionX:functionX functionY:functionY fromPoint:self.layer.position toPoint:destinationPoint];
    anim.fillMode = kCAFillModeForwards;
    anim.removedOnCompletion = NO;
    if (!(timing.start < 0.01 && timing.duration < 0.01)) {
        anim.duration = timing.duration;
        anim.beginTime = timing.start;
    }
    
    //self.layer.position = destinationPoint;
    return anim;
}


#pragma mark AGMove Along Path
#pragma mark ---

- (CAAnimation*)AGMoveAlongPath:(CGPathRef)path{
    return [self AGMoveAlongPath:path withParametricFunction:AGLinearEase timing:(AGTiming){0,0}];
}
- (CAAnimation*)AGMoveAlongPath:(CGPathRef)path withParametricFunction:(KeyframeParametricBlockFunction)function{
    return [self AGMoveAlongPath:path withParametricFunction:function timing:(AGTiming){0,0}];
}
- (CAAnimation*)AGMoveAlongPath:(CGPathRef)path withParametricFunction:(KeyframeParametricBlockFunction)function duration:(float)duration{
    return [self AGMoveAlongPath:path withParametricFunction:function timing:(AGTiming){0,duration}];
}
- (CAAnimation*)AGMoveAlongPath:(CGPathRef)path withParametricFunction:(KeyframeParametricBlockFunction)function timing:(AGTiming)timing{
    CAAnimation *anim = [CAKeyframeAnimation animationWithPath:path function:function];
    anim.fillMode = kCAFillModeForwards;
    anim.removedOnCompletion = NO;
    if (!(timing.start < 0.01 && timing.duration < 0.01)) {
        anim.duration = timing.duration;
        anim.beginTime = timing.start;
    }
    return anim;
}

- (CAAnimation*)AGMoveAlongPath:(CGPathRef)path withParametricFunction:(KeyframeParametricBlockFunction)function timing:(AGTiming)timing withRotation:(BOOL)rotation{
    CAAnimation *anim = [CAKeyframeAnimation animationWithPath:path function:function];
    anim.fillMode = kCAFillModeForwards;
    anim.removedOnCompletion = NO;
    if (!(timing.start < 0.01 && timing.duration < 0.01)) {
        anim.duration = timing.duration;
        anim.beginTime = timing.start;
    }
    return anim;
}

#pragma mark AGRotate
#pragma mark ---

- (CAAnimation*)AGRotateTo:(CGFloat)angle{
    return [self AGRotateTo:angle withParametricFunction:AGLinearEase timing:(AGTiming){0,0}];
}

- (CAAnimation*)AGRotateTo:(CGFloat)angle withParametricFunction:(KeyframeParametricBlockFunction)function{
    return [self AGRotateTo:angle withParametricFunction:function timing:(AGTiming){0,0}];
}

- (CAAnimation*)AGRotateTo:(CGFloat)angle withParametricFunction:(KeyframeParametricBlockFunction)function duration:(float)duration{
    return [self AGRotateTo:angle withParametricFunction:function timing:(AGTiming){0,duration}];
}

- (CAAnimation*)AGRotateTo:(CGFloat)angle withParametricFunction:(KeyframeParametricBlockFunction)function timing:(AGTiming)timing{
    CAAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation" function:function fromValue:self.angle toValue:angle];
    anim.fillMode = kCAFillModeForwards;
    anim.removedOnCompletion = YES;
    if (!(timing.start < 0.01 && timing.duration < 0.01)) {
        anim.duration = timing.duration;
        anim.beginTime = timing.start;
    }
    self.transform = CGAffineTransformMakeRotation(angle);
    return anim;
}

#pragma mark AGScale
#pragma mark ---

- (CAAnimation*)AGScaleTo:(CGFloat)scale{
    return [self AGScaleTo:scale withParametricFunction:AGLinearEase timing:(AGTiming){0,0}];
}

- (CAAnimation*)AGScaleTo:(CGFloat)scale withParametricFunction:(KeyframeParametricBlockFunction)function{
    return [self AGScaleTo:scale withParametricFunction:function timing:(AGTiming){0,0}];
}
- (CAAnimation*)AGScaleTo:(CGFloat)scale withParametricFunction:(KeyframeParametricBlockFunction)function duration:(float)duration{
    return [self AGScaleTo:scale withParametricFunction:function timing:(AGTiming){0,duration}];
}

- (CAAnimation*)AGScaleTo:(CGFloat)scale withParametricFunction:(KeyframeParametricBlockFunction)function timing:(AGTiming)timing{
    CAAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale" function:function fromValue:[self scale] toValue:scale];
    anim.fillMode = kCAFillModeForwards;
    anim.removedOnCompletion = NO;
    if (!(timing.start < 0.01 && timing.duration < 0.01)) {
        //anim.duration = timing.duration;
        anim.beginTime = timing.start;
    }
    [self setScale:scale];
    return anim;
}

- (CAAnimation*)AGxyScaleTo:(CGScale)scale{
    [self AGxyScaleTo:scale withParametricFunction:AGLinearEase];
}
- (CAAnimation*)AGxyScaleTo:(CGScale)scale withParametricFunction:(KeyframeParametricBlockFunction)function repeatCount:(NSUInteger)count{
        //CAAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation" function:function fromValue:self.scale toValue:scale];
        //anim.fillMode = kCAFillModeForwards;
        //anim.removedOnCompletion = NO;
        //anim.repeatCount = count;
        // [self.layer addAnimation:anim forKey:@"animateLayer"];
    
}
- (void)AGxyScaleTo:(CGScale)scale withParametricFunction:(KeyframeParametricBlockFunction)function{
    [self AGxyScaleTo:scale withParametricFunction:function repeatCount:1];
}


- (CAAnimation*)AGFadeTo:(CGFloat)fade WithParametricFunction:(KeyframeParametricBlockFunction)function duration:(float)duration{
    CAAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"opacity" function:function fromValue:self.alpha toValue:fade];
    anim.fillMode = kCAFillModeForwards;
    anim.duration = duration;
    anim.removedOnCompletion = NO;
    [self.layer addAnimation:anim forKey:@"animateLayer"];
    self.alpha = fade;
    return anim;
}
- (void)AGFadeInWithParametricFunction:(KeyframeParametricBlockFunction)function{
    [self AGFadeTo:1.0 WithParametricFunction:function duration:1];
}

- (void)AGFadeOutWithParametricFunction:(KeyframeParametricBlockFunction)function{
    [self AGFadeTo:0.0 WithParametricFunction:function duration:1];
}

- (CAAnimationGroup*)AGSequence:(NSArray*)arrayOfActions durationAutoMode:(BOOL)mode{
    float startTime = 0.0;
    NSMutableArray *ar = [NSMutableArray array];
    if (mode) {
        ar = [NSMutableArray arrayWithArray:arrayOfActions];
    } else {
        for (CAAnimation *a in arrayOfActions) {
            a.beginTime = startTime;
            startTime += a.duration;
            [ar addObject:a];
        }
    }
    
    CAAnimationGroup* group = [CAAnimationGroup animation];
    group.removedOnCompletion = FALSE;
    group.fillMode = kCAFillModeForwards;
    [group setAnimations:ar];
    return group;

}
- (CAAnimationGroup*)AGSequence:(NSArray*)arrayOfActions{
    return [self AGSequence:arrayOfActions durationAutoMode:NO];
    
}

- (void)AGRunAction:(id)anim{
    [self.layer addAnimation:anim forKey:nil];
}

- (void)AGRunAction:(CAAnimation*)anim withTiming:(AGTiming)timing{
    anim.beginTime = timing.start;
    anim.duration = timing.duration;
}
- (void)AGRunAction:(id)anim repeating:(int)repetition{
    [self AGRunAction:anim repeating:repetition withKey:nil];
}
- (void)AGRunAction:(id)anim repeating:(int)repetition withKey:(NSString*)key{
    EulerAngle a = [self angle3d];
    CAAnimation *c = (CAAnimation*)anim;
    c.repeatCount = (float)repetition;
    [self.layer addAnimation:anim forKey:key];
    [self setAngle3d:a];
}

- (CAAnimationGroup*)AG3DTransformWithAngles:(EulerAngle)angles WithParametricFunction:(KeyframeParametricBlockFunction)function withDirection:(BOOL)direction{
    CATransform3D t3d = self.layer.transform;
    double x = fmod([[self.layer  valueForKeyPath:@"transform.rotation.x"] doubleValue],M_PI*2.0);
    NSLog(@"~Comming From %f",x);
    double y = fmod([[self.layer  valueForKeyPath:@"transform.rotation.y"] doubleValue],M_PI*2.0);
    double z = fmod([[self.layer  valueForKeyPath:@"transform.rotation.z"] doubleValue],M_PI*2.0);
    NSMutableArray *array = [NSMutableArray array];
    if (direction) {
        if ((x - angles.x) > M_PI) {
            CAKeyframeAnimation *a1 = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.x" function:function fromValue:x toValue:angles.x];
            
        }
    } else {
        
    }
    if (x > M_PI) {
        
    }
    NSArray *arr = @[
                       [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.y" function:function fromValue:y toValue:angles.y],
                       [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.x" function:function fromValue:x toValue:angles.x],
                       [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z" function:function fromValue:z toValue:angles.z],
                       
                       ];
    
    CAAnimationGroup* group = [CAAnimationGroup animation];
    group.removedOnCompletion = FALSE;
    group.fillMode = kCAFillModeForwards;
    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [group setAnimations:array];
}

- (CAAnimationGroup*)AG3DTransformWithAngles:(EulerAngle)angles WithParametricFunction:(KeyframeParametricBlockFunction)function{
    double x = [[self.layer  valueForKeyPath:@"transform.rotation.x"] doubleValue];
    NSLog(@"~Comming From %f",x);
    double y = [[self.layer  valueForKeyPath:@"transform.rotation.y"] doubleValue];
    double z = [[self.layer  valueForKeyPath:@"transform.rotation.z"] doubleValue];
    
    NSArray *array = @[
                       [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.y" function:function fromValue:y toValue:angles.y],
                       [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.x" function:function fromValue:x toValue:angles.x],
                       [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z" function:function fromValue:z toValue:angles.z],
                       
                       ];
    
    CAAnimationGroup* group = [CAAnimationGroup animation];
    group.removedOnCompletion = FALSE;
    group.fillMode = kCAFillModeForwards;
    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [group setAnimations:array];
    [self.layer setValue:[NSNumber numberWithFloat:angles.x] forKeyPath:@"transform.rotation.x"];
    [self.layer setValue:[NSNumber numberWithFloat:angles.y] forKeyPath:@"transform.rotation.y"];
    [self.layer setValue:[NSNumber numberWithFloat:angles.z] forKeyPath:@"transform.rotation.z"];
    //[self setScale:[self scale]];
    //self.layer.transform = CATransform3DRotate(self.layer.transform, angles.x, 1.0, 0.0, 0.0);
    //self.layer.transform = CATransform3DRotate(self.layer.transform, angles.y, 0.0, 1.0, 0.0);
    //self.layer.transform = CATransform3DRotate(self.layer.transform, angles.z, 0.0, 0.0, 1.0);

    return group;

}

- (CAAnimationGroup*)AG3DTransformTo:(CATransform3D)transform WithParametricFunction:(KeyframeParametricBlockFunction)function{
    
    CATransform3D t3d = self.layer.transform;
    NSLog(@"~~~~~~~~%@",NSStringFromCATransform3D(CATransform3DIdentity));
    double syt = sqrt(t3d.m11*t3d.m11 + t3d.m12*t3d.m12);
   
    double sy = sqrt(transform.m11*transform.m11 + transform.m12*transform.m12);

    CGFloat za,xa,ya,_za,_xa,_ya;
    za = atan2(transform.m12, transform.m11);
    ya = atan2(sy,transform.m13);
    xa = atan2(transform.m23, transform.m33);
    _za = atan2(t3d.m12, t3d.m11);
    _ya = atan2(syt,t3d.m13);
    _xa = atan2(t3d.m23, t3d.m33);
    
    EulerAngle froa = [[self class] getAnglesFromMatrix:t3d];
    EulerAngle toa = [[self class] getAnglesFromMatrix:transform];
    NSLog(@"Eulre an = x = %f  y = %f  z = %f",DEG(froa.x) ,DEG(froa.y),DEG(froa.z));
    NSLog(@"Eulre an = x = %f  y = %f  z = %f",DEG(toa.x),DEG(toa.y),DEG(toa.z));

    NSArray *array = @[
                       [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.y" function:function fromValue:froa.y toValue:toa.y],
                       [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.x" function:function fromValue:froa.x toValue:toa.x],
                       [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z" function:function fromValue:froa.z toValue:toa.z],
                       
                       ];
    
    CAAnimationGroup* group = [CAAnimationGroup animation];
    group.removedOnCompletion = FALSE;
    group.fillMode = kCAFillModeForwards;
    [group setAnimations:array];
    return group;
    
}

- (void)AGFadeIn{
    CALayer *layer = (CALayer*)self.layer;
    layer.opacity = 1.0;
}

- (void)AGFadeOut{
    self.layer.opacity = 0.0;
}

#pragma mark getter - setters

- (void)setAngle:(CGFloat)tangle{
    //self.angle = tangle;
    //EulerAngle a = [self angle3d];
    [self.layer setValue:[NSNumber numberWithFloat:tangle] forKey:@"transform.rotation"];
    //[self setAngle3d:a];
    //self.xyScale = self.xyScale;
}
-(void)setScale:(CGFloat)scale{
    //EulerAngle a = [self angle3d];
    //NSLog(@"Settin Scale to be %f",scale);
    self.transform = CGAffineTransformMakeScale(scale, scale);
    //NSLog(@"Setting Scale = %f",scale);
    //NSLog(@"Setting Scale = %f",[self scale]);
    
    
    //[self setAngle3d:a];
    
}
- (void)setPosition:(CGPoint)position{
    //EulerAngle a = [self angle3d];
    [self.layer setValue:[NSValue valueWithCGPoint:position] forKey:@"position"];
    //[self setAngle3d:a];
}
- (void)setXyScale:(CGScale)xyScale{
    self.xyScale = xyScale;
    //EulerAngle a = [self angle3d];
    self.transform = CGAffineTransformMakeScale(xyScale.sx, xyScale.sy);
    //[self setAngle3d:a];
}

- (CGFloat)angle{
    return atan2(self.transform.b, self.transform.a);
    return [[self.layer  valueForKeyPath:@"transform.rotation"] floatValue];
}
- (CGFloat)scale{
    CGAffineTransform t = self.transform;
    double h = sqrt(t.a*t.a + t.c*t.c);
    double v = sqrt(t.b*t.b + t.d*t.d);
    //NSLog(@"- %f  - %f",h,v);
    //NSLog(@"- %f  - %f",h,v);

    return (h+v)/2.0;
}
- (CGPoint)position{
    return CGPointMake(self.transform.tx, self.transform.ty);
}
- (CGScale)xyScale{
    
    return (CGScale){
        (CGFloat)sqrt(pow(self.transform.a,2)+pow(self.transform.c,2)),
        (CGFloat)sqrt(pow(self.transform.b,2)+pow(self.transform.d,2))
    };
}

- (EulerAngle)angle3d{
    EulerAngle a = (EulerAngle){
        fmod([[self.layer  valueForKeyPath:@"transform.rotation.x"] doubleValue],M_PI*2.0),
        fmod([[self.layer  valueForKeyPath:@"transform.rotation.y"] doubleValue],M_PI*2.0),
        fmod([[self.layer  valueForKeyPath:@"transform.rotation.z"] doubleValue],M_PI*2.0)
    };

    
    return a;
}

- (void)setAngle3d:(EulerAngle)angle3d{
    [self.layer setValue:[NSNumber numberWithFloat:angle3d.x] forKeyPath:@"transform.rotation.x"];
    [self.layer setValue:[NSNumber numberWithFloat:angle3d.y] forKeyPath:@"transform.rotation.y"];
    [self.layer setValue:[NSNumber numberWithFloat:angle3d.z] forKeyPath:@"transform.rotation.z"];

}

-(void)animationWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay easeOptions:(CACurvedAnimationFuction)easeOptions forKeyPath:(CAKeyPath)keyPath fromPoint:(CGPoint)fromValue toValue:(CGPoint)toValue started:(void (^)(void))started completion:(void (^)(void))completion{
    
    
    
    KeyframeParametricBlockFunction function = AGLinearEase;
    
    
    [CATransaction begin];
    [CATransaction
     setValue:[NSNumber numberWithFloat:duration]
     forKey:kCATransactionAnimationDuration];
    
        // make an animation
    
    CAAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"position" function:function fromPoint:fromValue toPoint:toValue];
    
    anim.delegate = self;
    anim.fillMode = kCAFillModeForwards;
    anim.removedOnCompletion = NO;
    
    [self.layer addAnimation:anim forKey:@"animate"];
    
    [CATransaction commit];
    
    completed = completion;
    
    
}
-(void)animationWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay easeOptions:(CACurvedAnimationFuction)easeOptions forKeyPath:(CAKeyPath)keyPath fromValue:(float)fromValue toValue:(float)toValue started:(void (^)(void))started completion:(void (^)(void))completion{
    started();
    
    KeyframeParametricBlockFunction function = AGLinearEase;

    [CATransaction begin];
    [CATransaction
     setValue:[NSNumber numberWithFloat:duration]
     forKey:kCATransactionAnimationDuration];
    
        // make an animation    
    
    CAAnimation *anim = [CAKeyframeAnimation
                          animationWithKeyPath:@"we"
                          function:function fromValue:fromValue   toValue:toValue];
        // use it
    anim.delegate = self;
    anim.fillMode = kCAFillModeForwards;
    anim.removedOnCompletion = YES;
    
    [self.layer addAnimation:anim forKey:@"animate"];
    
    [CATransaction commit];

    completed = completion;
    
}

- (void)animationDidStop:(CAAnimation*)anim finished:(BOOL)flag{
    
    completed();
    
    
    
}

@end
