//
//  AGView.h
//  AGView
//
//  Created by Gelan Almagro on 1/21/13.
//  Copyright (c) 2013 Gelan Almagro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+CurvedAnimation.h"

@interface AGView : UIView

@end
